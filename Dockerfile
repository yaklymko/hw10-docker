FROM node:12

COPY build ./build
COPY controllers ./controllers
COPY db ./db
COPY routes ./routes
COPY services ./services

COPY app.js ./
COPY config.js ./
COPY package*.json ./
RUN npm install

CMD ["node", "app.js"]
EXPOSE $PORT